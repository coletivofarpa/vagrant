#!/bin/bash

# Baixando a lista de pacotes e códigos fontes de software da distribuição Debian para uso pelo gerenciador de pacotes apt (salvo em /etc/apt/source.list)
sudo apt-get update

# Instalando softwares utilitários do sistema GNU dos quais o Docker depende para funcionar
sudo apt-get -y install \
   ca-certificates \
   curl \
   gnupg \
   lsb-release

# Baixando e configurando as chaves de autenticidade do pacote Docker
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# Baixando e escrevendo o link para os pacotes e fontes do Docker para uso pelo gerenciador de pacotes apt (salvo em /etc/apt/source.list.d/docker.list)
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Dando permisões para conferencia de autenticidade do pacote Docker
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Baixando as listas atualizadas de pacotes recem adicionadas ao gerenciador de pacotes apt
sudo apt-get update

# Insatalando o Docker, seu plugin Docker Compose e Interface de Linha de Comando (CLI)
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Adicionando o usuário vagrant ao grupo de usuário com acesso ao docker
sudo usermod -aG docker vagrant

# Instalando o gestor do iptables firewall ufw e o servidor de acesso via protocolo acesso seguro ssh
#sudo apt install ufw openssh-server

# Adicionando o openssh e sua porta 22 a lista de permissões de acesso ao firewall ufw 
#sudo ufw allow openssh

# Habilitando as funções do firewall ufw
#sudo ufw enable

